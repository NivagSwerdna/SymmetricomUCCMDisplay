## Arduino Mega2560 Powered Display for Symmetricom UCCM-P GPSDO

This project will contain the source for my GPSDO display

[Discussion Thread on EEVBlog](http://www.eevblog.com/forum/projects/a-look-at-my-symmetricom-gpsdo-(ocxo-furuno-receiver)/?all)

# Environment
## Software
* Arduino IDE 1.6.8

## Hardware
* Arduino 2560 Clone
* 2.4" TFT Display (with touch screen)
* MAX3232 RS232 Level converter module
