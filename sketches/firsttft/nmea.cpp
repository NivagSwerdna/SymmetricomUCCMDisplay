#include <Adafruit_TFTLCD.h> // Hardware-specific library

uint8_t bufferIndex = 0;
uint8_t buffer[128];

extern String newTimeType;
extern boolean dCurrentTimeType;
extern String newTime;
extern boolean dCurrentTime;
extern String newDate;
extern boolean dCurrentDate;
extern String newAltitude;
extern boolean dCurrentAltitude;
extern String newPosition;
extern boolean dCurrentPosition;
extern String newLocator;
extern boolean dLocator;


// Helper for co-ordinate conversion

void calcLocator(String &dst, double lat, double lon) {
  int o1, o2, o3;
  int a1, a2, a3;
  double remainder;
  // longitude
  remainder = lon + 180.0;
  o1 = (int)(remainder / 20.0);
  remainder = remainder - (double)o1 * 20.0;
  o2 = (int)(remainder / 2.0);
  remainder = remainder - 2.0 * (double)o2;
  o3 = (int)(12.0 * remainder);

  // latitude
  remainder = lat + 90.0;
  a1 = (int)(remainder / 10.0);
  remainder = remainder - (double)a1 * 10.0;
  a2 = (int)(remainder);
  remainder = remainder - (double)a2;
  a3 = (int)(24.0 * remainder);

  dst = (char)((char)o1 + 'A');
  dst += (char)((char)a1 + 'A');
  dst += (char)((char)o2 + '0');
  dst += (char)((char)a2 + '0');
  dst += (char)((char)o3 + 'A');
  dst += (char)((char)a3 + 'A');
}

enum MessageTypes {
  invalid, GPGGA, GPGSV, GPGSA, GPtps, GPrrm
};

void process() {

  if (bufferIndex > 2) {
    MessageTypes messageType = invalid;

    // Firstly Classify NMEA Message


    //           1         2         3         4         5
    // 0123456789012345678901234567890123456789012345678901234567890
    // $GPGGA,172855,5125.3961,N,00017.4514,W,1,05,02.85,000011.8,M,0046.9,M,,

    if (buffer[0] != '$') goto fail;

    if (buffer[1] == 'P') {
      if (bufferIndex < 10) goto fail;

      if (buffer[2] != 'F') goto fail;
      if (buffer[3] != 'E') goto fail;
      if (buffer[4] != 'C') goto fail;
      if (buffer[5] != ',') goto fail;
      if (buffer[6] != 'G') goto fail;
      if (buffer[7] != 'P') goto fail;

      if ((buffer[8] == 't') && (buffer[9] == 'p') && (buffer[10] == 's') )
      {
        messageType = GPtps;
      }
      else if ((buffer[8] == 'r') && (buffer[9] == 'r') && (buffer[10] == 'm') )
      {
        messageType = GPrrm;
      }
      else goto fail;

    } else if (buffer[1] == 'G') {
      if (bufferIndex < 5) goto fail;

      if (buffer[2] != 'P') goto fail;
      if (buffer[3] != 'G') goto fail;

      if ((buffer[4] == 'G') && (buffer[5] == 'A'))
      {
        messageType = GPGGA;
      }
      else if ((buffer[4] == 'S') && (buffer[5] == 'A'))
      {
        messageType = GPGSA;
      }
      else if ((buffer[4] == 'S') && (buffer[5] == 'V'))
      {
        messageType = GPGSV;
      }
      else goto fail;

    } else
    {
      messageType = invalid; goto fail;
    }

    //           1         2         3         4         5
    // 0123456789012345678901234567890123456789012345678901234567890
    // $GPGSV,3,1,10,02,31,066,33,06,09,028,39,12,34,082,30,14,29,230,00

    if (messageType == GPGSV) {

      //      Serial.print("GPGSV: ");
      //      Serial.write(buffer[9]);
      //      Serial.print(" of ");
      //      Serial.write(buffer[7]);
      //      Serial.print(", #Vis: ");
      //      Serial.write(buffer[11]);
      //      Serial.write(buffer[12]);
      //      Serial.print(", SVNs: ");
      //      int offset = 14;
      //      for (int i = 0; i < 4 ; i++)
      //      {
      //        Serial.write(buffer[offset++]);
      //        Serial.write(buffer[offset++]);
      //        offset++;
      //
      //        Serial.print("(");
      //
      //        Serial.write(buffer[offset++]);
      //        Serial.write(buffer[offset++]);
      //        offset++;
      //
      //        Serial.print(",");
      //
      //        Serial.write(buffer[offset++]);
      //        Serial.write(buffer[offset++]);
      //        Serial.write(buffer[offset++]);
      //        offset++;
      //
      //        Serial.print(",");
      //
      //        Serial.write(buffer[offset++]);
      //        Serial.write(buffer[offset++]);
      //
      //        Serial.print(")");
      //
      //        if (buffer[offset] != ',') break;
      //
      //        offset++;
      //
      //        Serial.print(" ");
      //
      //      }
      //      Serial.println();
    }


    //           1         2         3         4         5
    // 0123456789012345678901234567890123456789012345678901234567890
    // $GPGGA,172855,5125.3961,N,00017.4514,W,1,05,02.85,000011.8,M,0046.9,M,,

    if (messageType == GPGGA) {

      //      Serial.print("GPGGA: ");
      //      Serial.write(buffer[24]);     // N or S
      //      Serial.print(" ");
      //      Serial.write(buffer[14]);     // 00 to 90
      //      Serial.write(buffer[15]);
      //      Serial.print(" ");
      //      Serial.write(buffer[16]);   // M
      //      Serial.write(buffer[17]);   // M
      //      Serial.write(buffer[18]);   // .
      //      Serial.write(buffer[19]);  // fM
      //      Serial.write(buffer[20]);  // fM
      //      Serial.write(buffer[21]);  // fM
      //      Serial.write(buffer[22]);  // fM
      //      Serial.print(" ");
      //      Serial.write(buffer[37]);   // E or W
      //      Serial.print(" ");
      //      Serial.write(buffer[26]);   // 000 to 180
      //      Serial.write(buffer[27]);
      //      Serial.write(buffer[28]);
      //      Serial.print(" ");
      //      Serial.write(buffer[29]);   // M
      //      Serial.write(buffer[30]);   // M
      //      Serial.write(buffer[31]);   // .
      //      Serial.write(buffer[32]);  // fM
      //      Serial.write(buffer[33]);  // fM
      //      Serial.write(buffer[34]);  // fM
      //      Serial.write(buffer[35]);  // fM
      //
      //      Serial.print(", Sats:");
      //      Serial.write(buffer[41]);
      //      Serial.write(buffer[42]);
      //
      //      Serial.print(", DOP: ");
      //
      //      Serial.write(buffer[44]);
      //      Serial.write(buffer[45]);
      //      Serial.write(buffer[46]);
      //      Serial.write(buffer[47]);
      //      Serial.write(buffer[48]);
      //      Serial.print(", Alt: ");
      //      Serial.write(buffer[50]);
      //      Serial.write(buffer[51]);
      //      Serial.write(buffer[52]);
      //      Serial.write(buffer[53]);
      //      Serial.write(buffer[54]);
      //      Serial.write(buffer[55]);
      //      Serial.write(buffer[56]);
      //      Serial.write(buffer[57]);
      //      Serial.println();

      String gpsPosition = "";
      gpsPosition += (char)buffer[24];  // N or S
      gpsPosition += (char)buffer[14];  // 00 to 90
      gpsPosition += (char)buffer[15];  //
      gpsPosition += " ";
      gpsPosition += (char)buffer[16];  // MM
      gpsPosition += (char)buffer[17];  //
      gpsPosition += ".";
      gpsPosition += (char)buffer[19];  // mmmm
      gpsPosition += (char)buffer[20];  //
      gpsPosition += (char)buffer[21];  //
      gpsPosition += (char)buffer[22];  //

      gpsPosition += " ";
      gpsPosition += " ";

      gpsPosition += (char)buffer[37];  // E or W
      gpsPosition += (char)buffer[26];  // 000 to 180
      gpsPosition += (char)buffer[27];  //
      gpsPosition += (char)buffer[28];  //
      gpsPosition += " ";
      gpsPosition += (char)buffer[29];  // MM
      gpsPosition += (char)buffer[30];  //
      gpsPosition += ".";
      gpsPosition += (char)buffer[32];  // mmmm
      gpsPosition += (char)buffer[33];  //
      gpsPosition += (char)buffer[34];  //
      gpsPosition += (char)buffer[35];  //

      newPosition = gpsPosition;
      dCurrentPosition = true;

      // Calculate Locator...

      int latDegrees = (int)((char)gpsPosition[1] - '0') * 10;
      latDegrees += (int)((char)gpsPosition[2] - '0');

      int latMins =  (int)((char)gpsPosition[4] - '0') * 10;
      latMins += (int)((char)gpsPosition[5] - '0');

      int latFracMins =  (int)((char)gpsPosition[7] - '0') * 1000;
      latFracMins += (int)((char)gpsPosition[8] - '0') * 100;
      latFracMins += (int)((char)gpsPosition[9] - '0') * 10;
      latFracMins += (int)((char)gpsPosition[10] - '0');

      double latitude = (double)latDegrees + ((double)latMins) / 60.0d + ((double)latFracMins) / 600000.0d;
      if ((char)gpsPosition[0] == 'S') latitude = -latitude;

      int longDegrees = (int)((char)gpsPosition[14] - '0') * 100;
      longDegrees += (int)((char)gpsPosition[15] - '0') * 10;
      longDegrees += (int)((char)gpsPosition[16] - '0');

      int longMins =  (int)((char)gpsPosition[18] - '0') * 10;
      longMins += (int)((char)gpsPosition[19] - '0');

      int longFracMins =  (int)((char)gpsPosition[21] - '0') * 1000;
      longFracMins += (int)((char)gpsPosition[22] - '0') * 100;
      longFracMins += (int)((char)gpsPosition[23] - '0') * 10;
      longFracMins += (int)((char)gpsPosition[24] - '0');

      double longitude = (double)longDegrees + ((double)longMins) / 60.0d + ((double)longFracMins) / 600000.0d;
      if ((char)gpsPosition[13] == 'W') longitude = -longitude;

      calcLocator(newLocator, latitude, longitude);
      dLocator = true;

      String gpsAltitude = "Alt: ";
      gpsAltitude += (char)buffer[50];
      gpsAltitude += (char)buffer[51];
      gpsAltitude += (char)buffer[52];
      gpsAltitude += (char)buffer[53];
      gpsAltitude += (char)buffer[54];
      gpsAltitude += (char)buffer[55];
      gpsAltitude += (char)buffer[56];
      gpsAltitude += (char)buffer[57];
      gpsAltitude += 'm';

      newAltitude = gpsAltitude;
      dCurrentAltitude = true;
    }


    //           1         2         3         4         5
    // 0123456789012345678901234567890123456789012345678901234567890
    // $GPGSA,A,3,02,06,25,26,31,,,,,,,,02.85,02.27,01.72

    if (messageType == GPGSA) {

      //      Serial.print("GPGSA: ");
      //      switch (buffer[7]) {
      //        case 'M' : Serial.print("2D-only Mode"); break;
      //        case 'A' : Serial.print("2D/3D Autoswitch Mode"); break;
      //      }
      //      Serial.print(" ");
      //      switch (buffer[9]) {
      //        case '1' : Serial.print("No Fix"); break;
      //        case '2' : Serial.print("2D Fix"); break;
      //        case '3' : Serial.print("3D Fix"); break;
      //      }
      //      Serial.print(", SVN: ");
      //      int offset = 11;
      //      for (int i = 0; i < 12; i++) {
      //        if (buffer[offset] != ',') {
      //          Serial.write(buffer[offset]);
      //          Serial.write(buffer[offset + 1]);
      //          Serial.print(" ");
      //          offset += 2;
      //        }
      //        offset++;
      //      }
      //      Serial.print(", PDOP: ");
      //      Serial.write(buffer[offset++]);
      //      Serial.write(buffer[offset++]);
      //      Serial.write(buffer[offset++]);
      //      Serial.write(buffer[offset++]);
      //      Serial.write(buffer[offset++]);
      //      offset++;  // Skip Comma
      //      Serial.print(", HDOP: ");
      //      Serial.write(buffer[offset++]);
      //      Serial.write(buffer[offset++]);
      //      Serial.write(buffer[offset++]);
      //      Serial.write(buffer[offset++]);
      //      Serial.write(buffer[offset++]);
      //      offset++;  // Skip Comma
      //      Serial.print(", VDOP: ");
      //      Serial.write(buffer[offset++]);
      //      Serial.write(buffer[offset++]);
      //      Serial.write(buffer[offset++]);
      //      Serial.write(buffer[offset++]);
      //      Serial.write(buffer[offset++]);
      //      Serial.println();
    }

    //           1         2         3         4         5
    // 0123456789012345678901234567890123456789012345678901234567890
    // $PFEC,GPtps,160329175303,3,1,1,150701000000,00,17,160329172124,1890,237200

    if (messageType == GPtps) {

      //      Serial.write(buffer[16]);   // D
      //      Serial.write(buffer[17]);   // D
      //      Serial.print("/");
      //      Serial.write(buffer[14]);   // M
      //      Serial.write(buffer[15]);   // M
      //      Serial.print("/20");
      //      Serial.write(buffer[12]);   // Y
      //      Serial.write(buffer[13]);   // Y
      //      Serial.print(" ");
      //      Serial.write(buffer[18]);   // D
      //      Serial.write(buffer[19]);   // D
      //      Serial.print(":");
      //      Serial.write(buffer[20]);   // D
      //      Serial.write(buffer[21]);   // D
      //      Serial.print(":");
      //      Serial.write(buffer[22]);   // D
      //      Serial.write(buffer[23]);   // D
      //      Serial.print(" ");
      //      switch (buffer[25]) {
      //        case '1' : Serial.print("RTC"); break;
      //        case '2' : Serial.print("GPS"); break;
      //        case '3' : Serial.print("UTC"); break;
      //      }
      //      Serial.print(" ");
      //
      //      switch (buffer[29]) {
      //        case '1' : Serial.print("Estimated OPM"); break;
      //        case '2' : Serial.print("Fixed OPM"); break;
      //      }
      //      Serial.println();

      if (buffer[25] == '3') {
        String fakeMessage = "$GPRMC,";
        fakeMessage += (char)buffer[18];
        fakeMessage += (char)buffer[19];
        fakeMessage += (char)buffer[20];
        fakeMessage += (char)buffer[21];
        fakeMessage += (char)buffer[22];
        fakeMessage += (char)buffer[23];
        fakeMessage += ",A,0.0,N,0.0,W,0.0,0.0,";
        fakeMessage += (char)buffer[16];
        fakeMessage += (char)buffer[17];
        fakeMessage += (char)buffer[14];
        fakeMessage += (char)buffer[15];
//        fakeMessage += "20";
        fakeMessage += (char)buffer[12];
        fakeMessage += (char)buffer[13];
        fakeMessage += ",0.0,E*";

        Serial1.print(fakeMessage);

        // Calculate Checksum....
        // Skip initial $ and last *
        unsigned char sum=0;
        for (int i=1; i<fakeMessage.length()-1; i++) {
          sum ^= fakeMessage.charAt(i);
        }

        if (sum<16) Serial1.print("0");
        Serial1.println(sum, HEX);
      }
      
      String gpsDate = "";
      gpsDate += (char)buffer[16];
      gpsDate += (char)buffer[17];
      gpsDate += "/";
      gpsDate += (char)buffer[14];
      gpsDate += (char)buffer[15];
      gpsDate += "/20";
      gpsDate += (char)buffer[12];
      gpsDate += (char)buffer[13];

      newDate = gpsDate;
      dCurrentDate = true;

      String gpsTime = "";
      gpsTime += (char)buffer[18];
      gpsTime += (char)buffer[19];
      gpsTime += ":";
      gpsTime += (char)buffer[20];
      gpsTime += (char)buffer[21];
      gpsTime += ":";
      gpsTime += (char)buffer[22];
      gpsTime += (char)buffer[23];

      newTime = gpsTime;
      dCurrentTime = true;

      String gpsTimeType = "";
      switch (buffer[25]) {
        case '1' : gpsTimeType = "RTC"; break;
        case '2' : gpsTimeType = "GPS"; break;
        case '3' : gpsTimeType = "UTC"; break;
      }

      newTimeType = gpsTimeType;
      dCurrentTimeType = true;

    }
  }

fail:
  bufferIndex = 0;
}

void push(uint8_t c) {
  if (bufferIndex < 128) {
    buffer[bufferIndex] = c;
    bufferIndex++;
  }

  if (c == 10) process();
}

