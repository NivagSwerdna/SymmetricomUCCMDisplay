void smartRewrite(Adafruit_TFTLCD &tft, uint8_t ts, uint16_t fgc, uint16_t bgc, int16_t x, int16_t y, String &oldValue, String &newValue);
