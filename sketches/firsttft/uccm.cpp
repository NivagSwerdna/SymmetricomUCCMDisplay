#include <Adafruit_TFTLCD.h> // Hardware-specific library

extern boolean dCurrentFFOM;
extern String newFFOM;
extern String currentFFOM;

extern boolean dCurrentTFOM;
extern String newTFOM;
extern String currentTFOM;

extern boolean dCurrentHold;
extern String newHold;
extern String currentHold;

extern boolean dCurrentSurveyProgress;
extern String newSurveyProgress;
extern String currentSurveyProgress;

extern boolean dDiagLoopPhase;
extern String newDiagLoopPhase;
extern String currentDiagLoopPhase;

extern boolean dDiagLoopGPS;
extern String newDiagLoopGPS;
extern String currentDiagLoopGPS;

extern boolean dDiagLoopFreq;
extern String newDiagLoopFreq;
extern String currentDiagLoopFreq;

extern boolean dDiagLoopTemp;
extern String newDiagLoopTemp;
extern String currentDiagLoopTemp;

extern boolean dUCCMPosition;
extern String newUCCMPosition;
extern String currentUCCMPosition;

bool atPrompt = false;

int promptPhase = 0;
char pattern[] = { 13, 10, 85, 67, 67, 77, 45, 80, 32, 62, 32 };

char uccmOutput[2048];
int writeIndex = 0;
int readIndex = 0;

void pushBuffer(char c) {
  uccmOutput[writeIndex] = c;
  writeIndex++;
  if (writeIndex >= 2048) writeIndex = 0;

  if (writeIndex == readIndex) {
    readIndex++;
    if (readIndex >= 2048) readIndex = 0;
  }
}

void clearBuffer() {
  writeIndex = 0;
  readIndex = 0;
}

unsigned int bufferLength() {
  if (writeIndex == readIndex) {
    return 0;
  }
  else if (writeIndex > readIndex) {
    return writeIndex - readIndex;
  }
  else
  {
    return writeIndex + 2048 - readIndex;
  }
}

char getBufferChar(int offset) {
  int realOffset = readIndex + offset;
  if (realOffset >= 2048) realOffset -= 2048;
  return uccmOutput[realOffset];
}

int getNumLines() {
  int lines = 0;
  for (int i = 0; i < bufferLength(); i++) {
    char c = getBufferChar(i);
    if (c == 10) lines++;
  }
  return lines;
}

void extractLine(int n, String &s) {

  int foundLines = 0;
  int index = 0;

  int linesToSkip = n;

  while (linesToSkip > foundLines) {
    char c = getBufferChar(index);
    if (c == 10) foundLines++;
    index++;
  }

  char c = getBufferChar(index);

  while (c != 13) {
    s += c;
    index++;
    c = getBufferChar(index);
  }
}




bool atUCCMPrompt() {
  return atPrompt;
}

bool promptDetector(char c) {

  atPrompt = false;

  if (c != pattern[promptPhase]) {
    promptPhase = 0;
    return atPrompt;
  }

  promptPhase++;

  if (promptPhase == 11) {
    promptPhase = 0;
    atPrompt = true;
    Serial.print("@");
    Serial.print(bufferLength());
    Serial.print("@");
  }

  return atPrompt;
}

void relayUCCMSerial()
{
  while (Serial3.available()) {

    int inByte = Serial3.read();
    Serial.write(inByte);
    promptDetector(inByte);
    pushBuffer(inByte);
  }
}


int commandCount = 0;
enum commandState { IDLE, WAIT_PROMPT, WAIT_RESPONSE, SEND_COMMAND, COMPLETED, DELAY };
commandState currentCommandState = IDLE;
unsigned long lastCommandTransition = 0L;

const char* commandToSend;

void sendCommand(const char* s) {

  commandToSend = s;
  currentCommandState = atPrompt ? WAIT_PROMPT : SEND_COMMAND;

  if (currentCommandState == WAIT_PROMPT) return;

  // Send Command
  atPrompt = false;
  currentCommandState = WAIT_RESPONSE;
  lastCommandTransition = millis();
  clearBuffer();
  Serial3.println(commandToSend);
  Serial.print("Sending: '");
  Serial.print(commandToSend);
  Serial.println("'");

}

boolean isIdle() {
  return currentCommandState == IDLE;
}




boolean commandCompleted() {
  if (currentCommandState == IDLE) return false;

  if (currentCommandState == WAIT_PROMPT) {
    if (atPrompt) {
      // Send Command
      atPrompt = false;
      currentCommandState = WAIT_RESPONSE;
      lastCommandTransition = millis();
      clearBuffer();
      Serial3.println(commandToSend);
    }
    else
    {
      unsigned long now = millis();
      if ((now - lastCommandTransition) > 2000L) {
        // Still not reached prompt after 2 seconds... let's kick it
        lastCommandTransition = now;
        Serial3.println("*CLS");
      }
    }
  }

  if (currentCommandState == WAIT_RESPONSE) {
    if (atPrompt) {
      currentCommandState = COMPLETED;
    } else
    {
      unsigned long now = millis();
      if ((now - lastCommandTransition) > 2000L) {
        // Still not reached prompt after 2 seconds... let's kick it
        lastCommandTransition = now;
        currentCommandState = WAIT_PROMPT;
        clearBuffer();
        Serial3.println("*CLS");
      }
    }
  }

  return (currentCommandState == COMPLETED);
}


int currentCommand = 0;
unsigned long lastCommandSent;
boolean queuedCommandWaiting = false;

void runUCCMCommands() {

  commandCompleted();

  if ((currentCommandState == IDLE) && (!queuedCommandWaiting)) {
    lastCommandSent = millis();
    switch (currentCommand) {
      case 0:
        sendCommand("SYNC:FFOM?");
        break;
      case 1:
        sendCommand("SYNC:TFOM?");
        break;
      case 2:
        sendCommand(":GPS:POS:SURV:STAT?");
        break;
      case 3:
        sendCommand(":GPS:POS:SURV:PROG?");
        break;
      case 4:
        sendCommand("DIAG:LOOP?");
        break;
      case 5:
        sendCommand("GPS:POS?");
        break;
    }
  }

  if (currentCommandState == COMPLETED) {

    // If not our command then dont process any further
    if (queuedCommandWaiting) return;

    // Dump Buffer
    //    Serial.println();
    //    Serial.print("Buffer: ");
    //    for (int i = 0; i < bufferLength(); i++) {
    //      char c = getBufferChar(i);
    //      if (c == 10) Serial.print("[LF]");
    //      else if (c == 13) Serial.print("[CR]");
    //      else Serial.print(c);
    //    }
    //    Serial.println();
    //    Serial.print("Lines: ");
    //    Serial.println(getNumLines());

    if ((currentCommand == 0) && (getNumLines() == 3)) {
      // Buffer: SYNC:TFOM?[CR][LF]10-100 ns[CR][LF]Command Complete[CR][LF]UCCM-P >
      dCurrentFFOM = true;
      newFFOM = "FFOM: ";
      extractLine(1, newFFOM);
    }
    else if ((currentCommand == 1) && (getNumLines() == 3)) {
      dCurrentTFOM = true;
      newTFOM = "TFOM: ";
      extractLine(1, newTFOM);
    }
    else if ((currentCommand == 2) && (getNumLines() == 3)) {
      dCurrentHold = true;
      newHold = "";
      extractLine(1, newHold);
      if (newHold == "0") {
        newHold = "Hold";
      } else
      {
        newHold = "Survey";
      }
    }
    else if ((currentCommand == 3) && (getNumLines() == 3)) {
      dCurrentSurveyProgress = true;
      newSurveyProgress = "Progress: ";
      extractLine(1, newSurveyProgress);
      newSurveyProgress += "%";
    }
    else if ((currentCommand == 4) && (getNumLines() == 16)) {

      dDiagLoopFreq = true;
      newDiagLoopFreq = "";
      extractLine(11, newDiagLoopFreq);

      dDiagLoopPhase = true;
      newDiagLoopPhase = "";
      extractLine(12, newDiagLoopPhase);

      dDiagLoopGPS = true;
      newDiagLoopGPS = "";
      extractLine(13, newDiagLoopGPS);

      dDiagLoopTemp = true;
      newDiagLoopTemp = "";
      extractLine(14, newDiagLoopTemp);

      Serial.println();
      Serial.print("Extracted Phase: ");
      Serial.println(newDiagLoopPhase);
    }
    else if ((currentCommand == 5) && (getNumLines() == 3)) {
      newUCCMPosition = "";
      extractLine(1, newUCCMPosition);
      dUCCMPosition = !currentUCCMPosition.equals(newUCCMPosition);
    }

    // Advance to next command
    currentCommand++;
    if (currentCommand > 5) currentCommand = 0;

    lastCommandSent = millis();
    currentCommandState = queuedCommandWaiting ? IDLE : DELAY;
  }

  if (currentCommandState == DELAY) {
    unsigned long now = millis();
    if ((now - lastCommandSent) >= 1000) {

      currentCommandState = IDLE;
    }
  }
}

const char* queuedCommand;

void issueCommand(const char* s) {
  // We have been given a command to execute and have to fit it in with the pending cycle...
  queuedCommand = s;
  queuedCommandWaiting = true;

  Serial.println("Wait Idle");

  // Wait until we are idle again...
  while (!isIdle()) {
    relayUCCMSerial();
    runUCCMCommands();
  }

  Serial.println();
  Serial.print("Queued Command: ");
  Serial.println(queuedCommand);

  sendCommand(queuedCommand);

  Serial.println("Wait Idle2");

  // Wait until we are done...
  while ((currentCommandState != COMPLETED) && (currentCommandState != IDLE)) {
    relayUCCMSerial();
    runUCCMCommands();
  }
  Serial.println("Wait Idle3");

  // Let other commands continue
  queuedCommandWaiting = false;

  currentCommandState = IDLE;

}


