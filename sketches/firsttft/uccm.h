bool promptDetector(char c);
bool atUCCMPrompt();
void relayUCCMSerial();
void runUCCMCommands();
boolean commandCompleted();
void sendCommand(const char* s);
void issueCommand(const char* s);
boolean isIdle();
