#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library

/* This can be optimised later to only erase different characters */

void smartRewrite(Adafruit_TFTLCD &tft, uint8_t ts, uint16_t fgc, uint16_t bgc, int16_t x, int16_t y, String &oldValue, String &newValue) {

  if (newValue.equals(oldValue)) return;

  if (oldValue.length() == newValue.length())
  {
    int cursorPerChar = ts * 6;
    int cursorPos = x;

     tft.setTextSize(ts);

    // Optimise where just a couple of characters are changing
    for (int i = 0; i < oldValue.length(); i++) {
      if (oldValue[i] != newValue[i]) {
        tft.setTextColor(bgc);
        tft.setCursor(cursorPos, y);
        tft.print((char)oldValue[i]);
        tft.setCursor(cursorPos, y);
        tft.setTextColor(fgc);
        tft.print((char)newValue[i]);
      }
      cursorPos += cursorPerChar;
    }
  }
  else
  {
    tft.setTextColor(bgc);
    tft.setTextSize(ts);
    tft.setCursor(x, y);
    for (int i = 0; i < oldValue.length(); i++)
      tft.print((char)oldValue[i]);

    tft.setCursor(x, y);
    tft.setTextColor(fgc);
    for (int i = 0; i < newValue.length(); i++)
      tft.print((char)newValue[i]);
  }
}

