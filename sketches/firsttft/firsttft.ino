#include <Adafruit_GFX.h>
#include <gfxfont.h>

#include <pin_magic.h>
#include <registers.h>

#include <Adafruit_TFTLCD.h> // Hardware-specific library
#include "TouchScreen.h"

#include "helper.h"
#include "nmea.h"
#include "uccm.h"

#include <EEPROM.h>

#define LCD_RD A0 // LCD Read goes to Analog 0
#define LCD_WR A1 // LCD Write goes to Analog 1
#define LCD_CD A2 // Command/Data goes to Analog 2
#define LCD_CS A3 // Chip Select goes to Analog 3
#define LCD_RESET A4 // Can alternately just connect to Arduino's reset pin

// When using the BREAKOUT BOARD only, use these 8 data lines to the LCD:
// For the Arduino Uno, Duemilanove, Diecimila, etc.:
//   D0 connects to digital pin 8  (Notice these are
//   D1 connects to digital pin 9   NOT in order!)
//   D2 connects to digital pin 2
//   D3 connects to digital pin 3
//   D4 connects to digital pin 4
//   D5 connects to digital pin 5
//   D6 connects to digital pin 6
//   D7 connects to digital pin 7
// For the Arduino Mega, use digital pins 22 through 29
// (on the 2-row header at the end of the board).

// Assign human-readable names to some common 16-bit color values:
#define  BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);
// If using the shield, all control and data lines are fixed, and
// a simpler declaration can optionally be used:
// Adafruit_TFTLCD tft;



#define YP A2  // must be an analog pin, use "An" notation!
#define XM A3  // must be an analog pin, use "An" notation!
#define YM 8   // can be a digital pin
#define XP 9   // can be a digital pin

// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 300 ohms across the X plate
TouchScreen ts = TouchScreen(XP, YP, XM, YM);

int currentPage = 1;

/** Variables shared with NMEA decoder **/
boolean dCurrentPosition = false;
String newPosition = "";
String currentPosition = "";

boolean dCurrentAltitude = false;
String newAltitude = "";
String currentAltitude = "";

boolean dCurrentDate = false;
String newDate = "";
String currentDate = "";

boolean dCurrentTime = false;
String newTime = "";
String currentTime = "00:00:00";

boolean dCurrentTimeType = false;
String newTimeType = "";
String currentTimeType = "???";

String currentLocator = "??????";
String newLocator = "";
boolean dLocator = false;

boolean dCurrentFFOM = false;
String newFFOM = "";
String currentFFOM = "";

boolean dCurrentTFOM = false;
String newTFOM = "";
String currentTFOM = "";

boolean dCurrentHold =  false;
String newHold = "";
String currentHold = "";

boolean dCurrentSurveyProgress = false;
String newSurveyProgress = "";
String currentSurveyProgress = "";

boolean dDiagLoopFreq = false;
String newDiagLoopFreq = "";
String currentDiagLoopFreq = "freq cor  = -?.??????e-??";

boolean dDiagLoopPhase = false;
String newDiagLoopPhase = "";
String currentDiagLoopPhase = "phase cor = -?.??????e-??";

boolean dDiagLoopGPS = false;
String newDiagLoopGPS = "";
String currentDiagLoopGPS = "gps phase = ?.??????e-??";

boolean dDiagLoopTemp = false;
String newDiagLoopTemp = "";
String currentDiagLoopTemp = "temp cor  = -?.??????e-??";

boolean dUCCMPosition = false;
String newUCCMPosition = "";
String currentUCCMPosition = "?,+??,+??,+?.?????E+??,?,+?,+??,+?.?????E+??,+?.?????E+??";

String savedUCCMPosition;

TSPoint lastPressed;

boolean buttonDown = false;
boolean waitButtonUp = false;

int buttonX;
int buttonY;

void sampleTouchScreen() {

  // Wait until previous button down serviced
  if (buttonDown) return;

  // This is relatively slow...
  lastPressed = ts.getPoint();

  // Restore for Graphics
  pinMode(XP, OUTPUT);
  pinMode(XM, OUTPUT);
  pinMode(YP, OUTPUT);
  pinMode(YM, OUTPUT);

  // Min Pressure
  if (lastPressed.z < 250) lastPressed.z = 0;

  //  if (lastPressed.z != 0) {
  //    Serial.print("Point: ");
  //    Serial.print(lastPressed.x);
  //    Serial.print(",");
  //    Serial.print(lastPressed.y);
  //    Serial.print(",");
  //    Serial.println(lastPressed.z);
  //  }


  if (waitButtonUp) {
    if (lastPressed.z == 0) {
      waitButtonUp = false;
    }
    else
    {
      return;
    }
  }

  if (lastPressed.z != 0) {
    waitButtonUp = true;
    buttonDown = true;

    if (lastPressed.x > 675)
    {
      buttonX = 0;
    } else if (lastPressed.x < 435)
    {
      buttonX = 2;
    }
    else
    {
      buttonX = 1;
    }

    if (lastPressed.y > 652)
    {
      buttonY = 2;
    } else if (lastPressed.y < 437)
    {
      buttonY = 0;
    }
    else
    {
      buttonY = 1;
    }

    Serial.print("Button Down:" );
    Serial.print(lastPressed.x);
    Serial.print(" ");
    Serial.print(lastPressed.y);
    Serial.print(" [");
    Serial.print(buttonX);
    Serial.print(",");
    Serial.print(buttonY);
    Serial.println("]");

  }
}

void restoreFromEEPROM() {

  // Check EEPROM Initialised
  byte value1 = EEPROM.read(0);   // X
  byte value2 = EEPROM.read(1);   // Y
  byte value3 = EEPROM.read(2);   // Z
  byte value4 = EEPROM.read(3);   // Z
  byte value5 = EEPROM.read(4);   // Y

  bool initialised = true;
  if (value1 != 'X') initialised = false;
  if (value2 != 'Y') initialised = false;
  if (value3 != 'Z') initialised = false;
  if (value4 != 'Z') initialised = false;
  if (value5 != 'Y') initialised = false;

  if (!initialised) {

    // Init EEPROM for first time    
    savedUCCMPosition = "Unknown Lat            Unknown Long          Unknown Alt ";
    int addr = 100;
    for (int i = 0; i < savedUCCMPosition.length(); i++)
      EEPROM.put(addr++, savedUCCMPosition.charAt(i));

    // Now write magic number at front
    addr = 0;
    String magic = "XYZZY";
    for (int i = 0; i < magic.length(); i++)
      EEPROM.put(addr++, magic.charAt(i));
  } else
  {
    //Serial.println("EEPROM looks OK");

    savedUCCMPosition = "";
    int addr = 100;
    for (int i = 0; i < 57; i++) {
      byte c = EEPROM.read(addr++);
      savedUCCMPosition += (char)c;
    }
    // Serial.println("Restored:");
    // Serial.println(savedUCCMPosition);
  }  
}


unsigned long start;
int phase;

void setup(void) {


  Serial.begin(57600);  // USB
  Serial2.begin(9600);  // GPS RX
  Serial3.begin(57600); // UCCM
  Serial1.begin(9600);  // RPi

  restoreFromEEPROM();

  tft.reset();
  uint16_t id = tft.readID();
  tft.begin(id);

  tft.setRotation(1);
  renderPage(currentPage);

  // Used for testing
  start = millis();
  phase = 99;
}


void renderPage(int pageNumber) {

  tft.fillScreen(BLACK);

  if ((pageNumber == 1) || (pageNumber == 2)) {
    tft.setTextColor(WHITE);  tft.setTextSize(2);
    tft.setCursor(20, 0);
    tft.println("Symmetricom UCCM-P GPSDO");

    tft.setTextColor(YELLOW);  tft.setTextSize(5);
    tft.setCursor(40, 31);
    tft.print(currentTime);

    tft.setTextColor(YELLOW);  tft.setTextSize(2);
    tft.setCursor(280, 31);
    tft.println(currentTimeType);
  }

  if (pageNumber == 1) {
    tft.setTextColor(GREEN);  tft.setTextSize(2);
    tft.setCursor(10, 105);
    tft.println(currentPosition);

    tft.setTextColor(GREEN);  tft.setTextSize(2);
    tft.setCursor(2 * 2 * 6, 125);
    tft.println(currentLocator);

    tft.setTextColor(GREEN);  tft.setTextSize(2);
    tft.setCursor(80 + 3 * 2 * 6, 125);
    tft.println(currentAltitude);

    tft.setTextColor(YELLOW);  tft.setTextSize(2);
    tft.setCursor(100, 75);
    tft.println(currentDate);

    String empty = "";

    smartRewrite(tft, 2, WHITE, BLACK, 12, 155, empty, currentFFOM);
    smartRewrite(tft, 2, WHITE, BLACK, 12, 173, empty, currentTFOM);
    smartRewrite(tft, 2, WHITE, BLACK, 28, 205, empty, currentHold);
    smartRewrite(tft, 2, WHITE, BLACK, 128, 205, empty, currentSurveyProgress);

  } else if (pageNumber == 2) {

    String empty = "";

    String title = "Symmetricom UCCM-P GPSDO";
    smartRewrite(tft, 2, WHITE, BLACK, 20, 0, empty, title);
    String pageSubtitle = "Diagnostic Loop";
    smartRewrite(tft, 2, BLUE, BLACK, 70, 180, empty, pageSubtitle);

    smartRewrite(tft, 2, WHITE, BLACK, 12, 80, empty, currentDiagLoopFreq);
    smartRewrite(tft, 2, WHITE, BLACK, 12, 100, empty, currentDiagLoopPhase);
    smartRewrite(tft, 2, WHITE, BLACK, 12, 120, empty, currentDiagLoopGPS);
    smartRewrite(tft, 2, WHITE, BLACK, 12, 140, empty, currentDiagLoopTemp);

  } else if (pageNumber == 3) {

    String empty = "";

    String title = "Symmetricom UCCM-P GPSDO";
    smartRewrite(tft, 2, WHITE, BLACK, 20, 0, empty, title);
    String pageSubtitle = "GPS Hold Position";
    smartRewrite(tft, 2, BLUE, BLACK, 70, 18, empty, pageSubtitle);

    // If an update is pending then do it to save a refresh
    if (dUCCMPosition) {
      currentUCCMPosition = newUCCMPosition;
      dUCCMPosition = false;
    }

    {
      // Current Position is too long to fit on one line so split it up
      String line1 = currentUCCMPosition.substring(0, 21);
      String line2 = currentUCCMPosition.substring(23, 44);
      String line3 = currentUCCMPosition.substring(45);

      tft.fillRect(20, 50, 260, (16 + 2) * 3, BLACK);
      smartRewrite(tft, 2, YELLOW, BLACK, 20, 50, empty, line1);
      smartRewrite(tft, 2, YELLOW, BLACK, 20, 68, empty, line2);
      smartRewrite(tft, 2, YELLOW, BLACK, 20, 86, empty, line3);
    }

    {
      // Current Position is too long to fit on one line so split it up
      String line1 = savedUCCMPosition.substring(0, 21);
      String line2 = savedUCCMPosition.substring(23, 44);
      String line3 = savedUCCMPosition.substring(45);

      tft.fillRect(20, 130, 260, (16 + 2) * 3, BLACK);
      smartRewrite(tft, 2, YELLOW, BLACK, 20, 130, empty, line1);
      smartRewrite(tft, 2, YELLOW, BLACK, 20, 148, empty, line2);
      smartRewrite(tft, 2, YELLOW, BLACK, 20, 166, empty, line3);
    }

    // Mini Menu at Bottom

    tft.setTextColor(YELLOW);  tft.setTextSize(4);
    tft.setCursor(0, 206);
    tft.print("Load");
    tft.setCursor(115, 206);
    tft.print("Save");
    tft.setCursor(222, 206);
    tft.print("Srvy");

    tft.drawFastVLine(105, 202, 38, WHITE);
    tft.drawFastVLine(215, 202, 38, WHITE);
    tft.drawFastHLine(0, 202, 320, WHITE);


  } else
  {
    tft.setTextColor(YELLOW);  tft.setTextSize(4);
    tft.setCursor(0, 15);
    tft.print("Main");
    tft.setCursor(115, 15);
    tft.print("Diag");
    tft.setCursor(222, 15);
    tft.print("Hold");

    tft.setCursor(222, 194);
    tft.print("Back");

    tft.drawFastVLine(105, 0, 240, WHITE);
    tft.drawFastVLine(215, 0, 240, WHITE);
    tft.drawFastHLine(0, 80, 320, WHITE);
    tft.drawFastHLine(0, 160, 320, WHITE);

  }
}

void relaySerial() {

  // Forward PC Commands to UCCM
  while (Serial.available()) {
    int inByte = Serial.read();
    Serial3.write(inByte);
  }

  // Forward UCCM responses to PC
  relayUCCMSerial();

  // Data from GPS RX
  while (Serial2.available()) {
    int inByte = Serial2.read();
    //Serial.write(inByte);
    push((char)inByte);
  }
}

int touchCounter = 0;

void transferLEDs() {

  digitalWrite(49,1-digitalRead(66)); 
  digitalWrite(48,1-digitalRead(67));
  
  digitalWrite(47,1-digitalRead(64));
  digitalWrite(46,1-digitalRead(65));

  digitalWrite(45,1-digitalRead(63));
  digitalWrite(44,0);

  digitalWrite(43,0);
  digitalWrite(42,1-digitalRead(62));            // BOTTOM

}

void loop(void) {

  // Move this into a ISR sometime
  transferLEDs();

  // Send to and from Serial and UCCM
  relaySerial();

  if ((currentPage == 1) || (currentPage == 2)) {

    if (dCurrentTime) {
      smartRewrite(tft, 5, YELLOW, BLACK, 40, 31, currentTime, newTime);
      currentTime = newTime;
      dCurrentTime = false;
    }

    if (dCurrentTimeType) {
      smartRewrite(tft, 2, YELLOW, BLACK, 280, 31, currentTimeType, newTimeType);
      currentTimeType = newTimeType;
      dCurrentTimeType = false;
    }
  }

  if (currentPage == 1) {


    if (dCurrentDate) {
      smartRewrite(tft, 2, YELLOW, BLACK, 100, 75, currentDate, newDate);
      currentDate = newDate;
      dCurrentDate = false;
    }

    if (dCurrentPosition) {
      smartRewrite(tft, 2, GREEN, BLACK, 10, 105, currentPosition, newPosition);
      //Serial.println("UPOS");
      //Serial.println(newPosition);
      currentPosition = newPosition;
      dCurrentPosition = false;
    }

    if (dCurrentAltitude) {
      smartRewrite(tft, 2, GREEN, BLACK, 80 + 3 * 2 * 6, 125, currentAltitude, newAltitude);
      currentAltitude = newAltitude;
      dCurrentAltitude = false;
    }

    if (dLocator) {
      smartRewrite(tft, 2, GREEN, BLACK, 2 * 2 * 6, 125, currentLocator, newLocator);
      currentLocator = newLocator;
      dLocator = false;
    }

    if (dCurrentFFOM) {
      smartRewrite(tft, 2, WHITE, BLACK, 12, 155, currentFFOM, newFFOM);
      currentFFOM = newFFOM;
      dCurrentFFOM = false;
    }

    if (dCurrentTFOM) {
      smartRewrite(tft, 2, WHITE, BLACK, 12, 173, currentTFOM, newTFOM);
      currentTFOM = newTFOM;
      dCurrentTFOM = false;
    }

    if (dCurrentHold) {
      smartRewrite(tft, 2, WHITE, BLACK, 28, 205, currentHold, newHold);
      currentHold = newHold;
      dCurrentHold = false;
    }

    if (dCurrentSurveyProgress) {
      smartRewrite(tft, 2, WHITE, BLACK, 128, 205, currentSurveyProgress, newSurveyProgress);
      currentSurveyProgress = newSurveyProgress;
      dCurrentSurveyProgress = false;
    }
  }


  if (currentPage == 2) {

    if (dDiagLoopFreq) {
      smartRewrite(tft, 2, WHITE, BLACK, 12, 80, currentDiagLoopFreq, newDiagLoopFreq);
      currentDiagLoopFreq = newDiagLoopFreq;
      dDiagLoopFreq = false;
    }
    if (dDiagLoopPhase) {
      smartRewrite(tft, 2, WHITE, BLACK, 12, 100, currentDiagLoopPhase, newDiagLoopPhase);
      currentDiagLoopPhase = newDiagLoopPhase;
      dDiagLoopPhase = false;
    }
    if (dDiagLoopGPS) {
      smartRewrite(tft, 2, WHITE, BLACK, 12, 120, currentDiagLoopGPS, newDiagLoopGPS);
      currentDiagLoopGPS = newDiagLoopGPS;
      dDiagLoopGPS = false;
    }
    if (dDiagLoopTemp) {
      smartRewrite(tft, 2, WHITE, BLACK, 12, 140, currentDiagLoopTemp, newDiagLoopTemp);
      currentDiagLoopTemp = newDiagLoopTemp;
      dDiagLoopTemp = false;
    }
  }

  if (currentPage == 3) {

    if (dUCCMPosition) {
      // Current Position is too long to fit on one line so split it up

      String empty = "";

      String line1 = currentUCCMPosition.substring(0, 21);
      String line2 = currentUCCMPosition.substring(23, 44);
      String line3 = currentUCCMPosition.substring(45);

      tft.fillRect(20, 50, 260, (16 + 2) * 3, BLACK);
      smartRewrite(tft, 2, YELLOW, BLACK, 20, 50, empty, line1);
      smartRewrite(tft, 2, YELLOW, BLACK, 20, 68, empty, line2);
      smartRewrite(tft, 2, YELLOW, BLACK, 20, 86, empty, line3);

      currentUCCMPosition = newUCCMPosition;
      dUCCMPosition = false;
    }
  }


  runUCCMCommands();

  touchCounter++;
  if (touchCounter > 10) touchCounter = 0;
  if (touchCounter == 0) sampleTouchScreen();

  if (currentPage == 0) {
    if (buttonDown) {
      if ((buttonX == 0) && (buttonY == 0)) {
        // to Main
        currentPage = 1;
        renderPage(currentPage);
      } else if ((buttonX == 1) && (buttonY == 0)) {
        // to Diag
        currentPage = 2;
        renderPage(currentPage);
      } else if ((buttonX == 2) && (buttonY == 0)) {
        // to HOLD
        currentPage = 3;
        renderPage(currentPage);
      }
      else if ((buttonX == 2) && (buttonY == 2)) {
        // Back Button
        currentPage = 1;
        renderPage(currentPage);
      }

      buttonDown = false; // Consumed
    }
  }

  else if (currentPage == 3) {
    if (buttonDown) {
      if ((buttonX == 0) && (buttonY == 2)) {
        // Load
        
        String command = "GPS:POS ";
        command += savedUCCMPosition;
        issueCommand(command.c_str());
        
        renderPage(currentPage);        
        
      } else if ((buttonX == 1) && (buttonY == 2)) {
        // Save
        savedUCCMPosition = currentUCCMPosition;
        int addr = 100;
        for (int i = 0; i < savedUCCMPosition.length(); i++)
          EEPROM.put(addr++, savedUCCMPosition.charAt(i));                 
        renderPage(currentPage);        
      } else if ((buttonX == 2) && (buttonY == 2)) {
        // Survey
        String command = ":GPS:POS:SURV ONCE\r\n";
        issueCommand(command.c_str());       
        renderPage(currentPage);        
        
      } else
      {
        currentPage = 1;
        renderPage(currentPage);        
      }
      buttonDown = false; // Consumed
    }
  }
  else if ((currentPage == 1) || (currentPage == 2)) {
    if (buttonDown) {
      currentPage = 0;
      renderPage(currentPage);
      buttonDown = false; // Consumed
    }
  }
}
